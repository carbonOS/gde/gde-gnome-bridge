private void own_name(string name, owned BusAcquiredCallback? cb = null) {
    Bus.own_name(BusType.SESSION, name, BusNameOwnerFlags.NONE,
        (owned) cb, // on obtained
        null, // on lost
        () => error("Failed to own name: " + name) // on error
    );
}

int main(string[] args) {
    Gtk.init(ref args);

    var css_provider = new Gtk.CssProvider();
    css_provider.load_from_resource("/sh/carbon/gde-gnome-bridge/styles.css");
    Gtk.StyleContext.add_provider_for_screen(
        Gdk.Screen.get_default(), css_provider,
        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
    );
    Gtk.Settings.get_default().gtk_application_prefer_dark_theme = true;
    Gtk.Settings.get_default().gtk_theme_name = "adw-gtk3";

    // Setup the services
    own_name("org.gnome.Shell", conn => {
        // Register the services
        new GnomeShellService(conn);
        new EndSessionDialogService(conn);
        new DisplayConfigService(conn);
        new IdleWatchService(conn);
        new WfGsettingsSync();

        // Own extra names
        own_name("org.gnome.Mutter.DisplayConfig");
        own_name("org.gnome.Mutter.IdleMonitor");
    });

    Gtk.main();
    return 0;
}
