class WfGsettingsSync {
    public WfGsettingsSync() {
        var wf_input = new Settings("org.wayfire.section.input");

        // Mouse settings
        var g_mouse = new Settings("org.gnome.desktop.peripherals.mouse");
        //mouse: left-handed mode (swap l & r buttons)
        bind(g_mouse, "speed", wf_input, "mouse-cursor-speed");
        // natural-scroll (b)
        bind(g_mouse, "accel-profile", wf_input, "mouse-accel-profile");
        //mouse: middle click emulation
        //mouse: double click time

        // Touchpad settings
        var g_touchpad = new Settings("org.gnome.desktop.peripherals.touchpad");
        // touchpad: edge-scrolling-enabled (b)
        // touchpad: two-finger-scrolling-enabled (b)
        bind(g_touchpad, "disable-while-typing", wf_input,
            "disable-touchpad-while-typing");
        bind(g_touchpad, "tap-to-click", wf_input, "tap-to-click");
        // send-events (enum: enabled(0), disabled(1), disabled-on-external-mouse(2))
        bind(g_touchpad, "speed", wf_input, "touchpad-cursor-speed");
        bind(g_touchpad, "natural-scroll", wf_input, "natural-scroll");
        bind(g_touchpad, "middle-click-emulation", wf_input, "middle-emulation");
        bind(g_touchpad, "click-method", wf_input, "click-method",
            click_method => {
                switch (click_method as string) {
                    case "none":
                        return "none";
                    case "areas":
                        return "button-areas";
                    case "fingers":
                        return "clickfinger";
                    case "default":
                    default:
                        return "default";
                }
            });

        // Keyboard settings
        var g_keyboard = new Settings("org.gnome.desktop.peripherals.keyboard");
        bind(g_keyboard, "repeat-interval", wf_input, "kb-repeat-rate",
            transform_u2i);
        bind(g_keyboard, "delay", wf_input, "kb-repeat-delay", transform_u2i);
        bind(g_keyboard, "numlock-state", wf_input, "kb-numlock-default-state");
        if (!g_keyboard.get_boolean("repeat"))
            wf_input.set_int("kb-repeat-delay", -1);
        g_keyboard.changed["repeat"].connect(() => {
            int val = (int) g_keyboard.get_uint("delay");
            if (!g_keyboard.get_boolean("repeat")) val = -1;
            wf_input.set_int("kb-repeat-delay", val);
        });

        // Cursor settings
        var g_interface = new Settings("org.gnome.desktop.interface");
        bind(g_interface, "cursor-theme", wf_input, "cursor-theme");
        bind(g_interface, "cursor-size", wf_input, "cursor-size");

        // Input sources
        var g_input_sources = new Settings("org.gnome.desktop.input-sources");
        bind(g_input_sources, "xkb-options", wf_input, "xkb-options", opts => {
            var optsarr = opts as string[];
            var output = "";
            foreach (string opt in optsarr) {
                if (output != "") output += ",";
                output += opt;
            }
            return output;
        });
        // TODO: Actually selecting the input source


        // TODO in Wayfire
        // Per-window input sources
        //
        // (REQUESTED)
        // Missing swapping left & right keys
        // Missing double click time
        // Split natural scroll b/t mouse and touchpad
        // Split middle emulation b/t mouse and touchpad
        // Make it possible to use multiple different scroll types on touchpad
        // Make it possible to disable the touchpad

    }

    delegate Variant Transform(Variant a);

    private Variant transform_noop(Variant a) { // Does nothing
        return a;
    }

    private Variant transform_u2i(Variant a) { // Transforms uint -> int
        return (int)(a.get_uint32());
    }

    private void bind(Settings from, string from_id, Settings to, string to_id,
            Transform transform = transform_noop) {
        to.set_value(to_id, transform(from.get_value(from_id)));
        from.changed[from_id].connect(() =>
            to.set_value(to_id, transform(from.get_value(from_id))));
    }
}
