class MonitorLabler {

    private Gtk.Window[] visible = {};

    private class LabelWindow : Gtk.Window {
        public LabelWindow(int32 number, Gdk.Monitor monitor) {
            // Populate contents
            this.margin = 12;
            this.get_style_context().add_class("monitor-lbl");
            var lbl = new Gtk.Label("%d".printf(number));
            this.add(lbl);

            // Init layer shell
            GtkLayerShell.init_for_window(this);
            GtkLayerShell.set_layer(this, GtkLayerShell.Layer.TOP);
            GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.LEFT, true);
            GtkLayerShell.set_margin(this, GtkLayerShell.Edge.LEFT, this.margin);
            GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.TOP, true);
            GtkLayerShell.set_margin(this, GtkLayerShell.Edge.TOP, this.margin);
            GtkLayerShell.set_monitor(this, monitor);
            GtkLayerShell.set_keyboard_interactivity(this, false);

            // Show
            this.show_all();
        }
    }

    // Because we're stuck w/ GTK3, this is terrible
    private Gdk.Monitor? monitor_for_connector(Output[] outputs, string conn) {
        var display = Gdk.Display.get_default();
        foreach (var output in outputs)
            if (output.connector == conn)
                for (int i = 0; i < display.get_n_monitors(); i++) {
                    var monitor = display.get_monitor(i);

                    if (monitor.geometry.x == output.x &&
                        monitor.geometry.y == output.y)
                        return monitor;
                }
        return null;
    }

    public void show(HashTable<string, Variant> mapping) {
        if (visible.length > 0) hide();

        var outputs = DisplayConfig.get_outputs();
        var connectors = mapping.get_keys();
        foreach (var connector in connectors) {
            var number = mapping[connector] as int32;
            var monitor = monitor_for_connector(outputs, connector);
            if (monitor != null)
                visible += new LabelWindow(number, monitor);
        }
    }

    public void hide() {
        foreach (var win in visible)
            win.destroy();
        visible = {};
    }

}
