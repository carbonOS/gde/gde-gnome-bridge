class OSDMgr : Object {

    public class Popup : Gtk.Window {
        public string? ico { get; set; default = null; }
        public string? text { get; set; default = null; }
        public double level { get; set; default = -1.0; }

        construct {
            this.get_style_context().remove_class("background");

            var root = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 24);
            root.get_style_context().add_class("shell-osd");
            root.halign = Gtk.Align.CENTER;
            this.add(root);
            var image = new Gtk.Image();
            image.pixel_size = 24;
            root.pack_start(image, false, false);
            var vbox = new Gtk.Box(Gtk.Orientation.VERTICAL, 6);
            vbox.valign = Gtk.Align.CENTER;
            root.pack_start(vbox);
            var lbl = new Gtk.Label(null);
            lbl.get_style_context().add_class("title-4");
            vbox.add(lbl);
            var pbar = new Gtk.ProgressBar();
            pbar.width_request = 125;
            vbox.add(pbar);

            this.notify["ico"].connect(() => {
               image.visible = (ico != null);
               image.icon_name = ico;
            });
            this.notify["text"].connect(() => {
                lbl.visible = (text != null);
                vbox.visible = pbar.visible || lbl.visible;
                lbl.label = text;
            });
            this.notify["level"].connect(() => {
                pbar.visible = (level >= 0);
                vbox.visible = pbar.visible || lbl.visible;
                if (level >= 0) pbar.set_fraction(this.level);
            });

            // Init layer shell & create the window
            GtkLayerShell.init_for_window(this);
            GtkLayerShell.set_layer(this, GtkLayerShell.Layer.OVERLAY);
            GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.BOTTOM, true);
            GtkLayerShell.set_margin(this, GtkLayerShell.Edge.BOTTOM, 54);
            GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.LEFT, true);
            GtkLayerShell.set_anchor(this, GtkLayerShell.Edge.RIGHT, true);
            set_primary_monitor();
            GtkLayerShell.set_keyboard_interactivity(this, false);
            this.show_all();

            // Handle the initial values of the properties
            this.notify_property("ico");
            this.notify_property("text");
            this.notify_property("level");
        }

        private void set_primary_monitor() {
            var primary_settings = new Settings("sh.carbon.gde-gnome-bridge.output");
            var primary = primary_settings.get_string("primary-gtk3");
            var display = Gdk.Display.get_default();
            for (int i = 0; i < display.get_n_monitors(); i++) {
                var monitor = display.get_monitor(i);
                var compare = "%d,%d".printf(monitor.geometry.x,
                    monitor.geometry.y);
                if (compare == primary) {
                    GtkLayerShell.set_monitor(this, monitor);
                    return;
                }
            }
        }
    }

    private Popup? window = null;
    private uint close_timeout = 0;

    public void show(string? icon, string? text, double level) {
        // Show the window if needed
        if (window == null)
            window = new Popup();

        // Reschedule the close
        if (close_timeout != 0)
            Source.remove(close_timeout);
        close_timeout = Timeout.add(1500, () => {
            if (window != null) {
                window.destroy();
                window = null;
            }

            close_timeout = 0;
            return Source.REMOVE;
        });

        // Update the window
        window.ico = icon;
        window.text = text;
        window.level = level;
    }
}
