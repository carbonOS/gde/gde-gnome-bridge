[DBus (name = "org.gnome.Shell")]
class GnomeShellService : Object {
    private OSDMgr osd_mgr;
    private GrabAccelMgr accel_mgr;
    private MonitorLabler monitor_labler;

    public GnomeShellService(DBusConnection conn) {
        try {
            conn.register_object("/org/gnome/Shell", this);
        } catch {
            warning("Failed to register GnomeShellService");
            return;
        }

        osd_mgr = new OSDMgr();
        accel_mgr = new GrabAccelMgr();
        // TODO: Connect accel_mgr to accelerator_activated
        monitor_labler = new MonitorLabler();
    }

    public void eval(string cmd, out bool success, out string result) throws Error {
        warning("Eval called! cmd=%s", cmd);
        success = false;
        result = "";
        throw new DBusError.NOT_SUPPORTED("GDE cannot evaluate JavaScript");
    }

    // Name has special capitalization
    public void ShowOSD(HashTable<string, Variant> props) throws Error {
        var icon = props["icon"] as string?;
        var label = props["label"] as string?;
        var level = ("level" in props) ? props["level"] as double : -1.0;
        osd_mgr.show(icon, label, level);
    }

    public void show_applications() throws Error {
        // TODO: Do this properly
        Process.spawn_command_line_sync("gde-sig toggle_popover\\;launcher");
    }

    public void focus_search() throws Error {
        show_applications(); // Search and applications are in the same place
    }

    public uint32 grab_accelerator(string accelerator, uint32 mode_flags,
            uint32 grab_flags) throws Error {
        return accel_mgr.grab(accelerator, mode_flags, grab_flags);
    }

    public bool ungrab_accelerator(uint32 action) throws Error {
        return accel_mgr.ungrab(action);
    }

    public struct GAs_Accelerator {
        string accelerator;
        uint32 mode_flags;
        uint32 grab_flags;
    }

    public uint32[] grab_accelerators(GAs_Accelerator[] accels) throws Error {
        uint32[] output = {};
        foreach (var accel in accels) {
            output += grab_accelerator(accel.accelerator, accel.mode_flags,
                accel.grab_flags);
        }
        return output;
    }

    public bool ungrab_accelerators(uint32[] actions) throws Error {
        var success = true;
        foreach (var action in actions) {
            if (!ungrab_accelerator(action))
                success = false;
        }
        return success;
    }

    public signal void accelerator_activated(uint32 action, HashTable<string, Variant> props);

    public void show_monitor_labels(HashTable<string, Variant> props) throws Error {
        monitor_labler.show(props);
    }

    public void hide_monitor_labels() throws Error {
        monitor_labler.hide();
    }

    public void screen_transition() throws Error {
        print("DEBUG: org.gnome.Shell: ScreenTransition\n");
        // TODO
    }

    // Can be used to ID this as running on GDE
    public string shell_version { get; default = "GDE"; }
}
