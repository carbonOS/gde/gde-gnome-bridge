#include <glib-2.0/glib.h>
#include <glib-2.0/glib-object.h>
#include <gtk-3.0/gdk/gdkwayland.h>
#include <wayland-client.h>
#include <wayland-client-protocol.h>

#include "idle-client-protocol.h"

static void noop() {} // Does nothing

// Global instances of wayland resources
static struct org_kde_kwin_idle *idle_manager = NULL;
static struct wl_seat *seat = NULL;

// Interop functions (see protocol.vala)
void idle_monitor_emit_tick();
void idle_monitor_emit_activity();

// IDLE

static void handle_idle(void* data, struct org_kde_kwin_idle_timeout *timer) {
	idle_monitor_emit_tick();
}

static void handle_resume(void* data, struct org_kde_kwin_idle_timeout *timer) {
	idle_monitor_emit_activity();
}

static const struct org_kde_kwin_idle_timeout_listener timer_listener = {
	.idle = handle_idle,
	.resumed = handle_resume,
};

// REGISTRY & SETUP

static void registry_add(gpointer data, struct wl_registry *registry, guint32 id, const char *interface, guint32 version) {
  if (strcmp(interface, org_kde_kwin_idle_interface.name) == 0) {
    idle_manager = wl_registry_bind(registry, id, &org_kde_kwin_idle_interface, version);
  } else if (strcmp(interface, wl_seat_interface.name) == 0) {
    seat = wl_registry_bind(registry, id, &wl_seat_interface, version);
  }
}

static struct wl_registry_listener registry_listener = { &registry_add, &noop };

void idle_monitor_connect() {
  GdkDisplay *gdk_display = gdk_display_get_default();
  struct wl_display *display = gdk_wayland_display_get_wl_display(gdk_display);
  struct wl_registry *registry = wl_display_get_registry(display);
  wl_registry_add_listener(registry, &registry_listener, NULL);
  wl_display_roundtrip(display);
  wl_registry_destroy(registry);

  if (!idle_manager || !seat) return; // We can't do much

  struct org_kde_kwin_idle_timeout *timer =
    org_kde_kwin_idle_get_idle_timeout(idle_manager, seat, 10);
  if (!timer) return; // Again, can't do much

  org_kde_kwin_idle_timeout_add_listener(timer, &timer_listener, NULL);
}

