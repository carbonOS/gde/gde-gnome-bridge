namespace IdleMonitor {

    public delegate void IdleCallback();
    private IdleCallback on_tick = null;
    private IdleCallback on_user_activity = null;
    public void setup(owned IdleCallback tick_cb, owned IdleCallback activ_cb) {
        if (on_tick != null || on_user_activity != null) return;
        on_tick = (owned) tick_cb;
        on_user_activity = (owned) activ_cb;
        connect();
    }

    // C Interop functions

    public void emit_tick() {
        on_tick();
    }

    public void emit_activity() {
        on_user_activity();
    }

    extern void connect();
}
