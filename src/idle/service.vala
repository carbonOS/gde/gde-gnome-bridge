class IdleWatchService {

    public IdleWatchService(DBusConnection conn) {
        var obj_mgr = new DBusObjectManagerServer("/org/gnome/Mutter/IdleMonitor");

        var obj = new DBusObjectSkeleton("/org/gnome/Mutter/IdleMonitor/Core");
        obj.add_interface(new IdleWatchInterface());
        obj_mgr.export(obj);

        obj_mgr.set_connection(conn);
        obj_mgr.@ref(); // Don't destroy the obj manager right away
    }
}

class IdleWatchInterface : DBusInterfaceSkeleton {

    // DBus Boilerplate

    static DBusNodeInfo obj_info = null;

    construct {
        try {
            if (obj_info == null)
                obj_info = new DBusNodeInfo.for_xml("""
                <!DOCTYPE node PUBLIC
                '-//freedesktop//DTD D-BUS Object Introspection 1.0//EN'
                'http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd'>
                <node>
                  <!--
                      org.gnome.Mutter.IdleMonitor:
                      @short_description: idle monitor interface

                      This interface is used by gnome-desktop to implement
                      user activity monitoring.
                  -->

                  <interface name="org.gnome.Mutter.IdleMonitor">
                    <method name="GetIdletime">
                      <arg name="idletime" direction="out" type="t"/>
                    </method>

                    <method name="AddIdleWatch">
                      <arg name="interval" direction="in" type="t" />
                      <arg name="id" direction="out" type="u" />
                    </method>

                    <method name="AddUserActiveWatch">
                      <arg name="id" direction="out" type="u" />
                    </method>

                    <method name="RemoveWatch">
                      <arg name="id" direction="in" type="u" />
                    </method>

                    <method name="ResetIdletime"/>

                    <signal name="WatchFired">
                      <arg name="id" direction="out" type="u" />
                    </signal>
                  </interface>
                </node>
                """);
        } catch {
            error("Couldn't create idle obj_info");
        }

        watch_fired.connect(id => {
            try {
                this.get_connection().emit_signal(null, this.get_object_path(),
                    "org.gnome.Mutter.IdleMonitor", "WatchFired",
                    new Variant("(u)", id));
            } catch {
                warning("Failed to emit WatchFired");
            }
        });

        IdleMonitor.setup(_on_idletime_tick, _on_user_activity);
    }

    public override unowned DBusInterfaceInfo get_info() {
        return obj_info.lookup_interface("org.gnome.Mutter.IdleMonitor");
    }

    public override Variant get_properties() {
        return new Variant("a{sv}");
    }

    public override DBusInterfaceVTable? get_vtable() {
        return { on_dbus_call, null, null };
    }

    public void on_dbus_call(DBusConnection conn, string sender,
                             string object_path, string interface_name,
                             string method_name, Variant parameters,
                             owned DBusMethodInvocation invocation) {
        switch (method_name) {
            case "GetIdletime":
                invocation.return_value(new Variant("(t)", get_idletime()));
                break;
            case "AddIdleWatch":
                uint64 interval_param;
                parameters.get("(t)", out interval_param);
                var return_val = add_idle_watch(interval_param);
                invocation.return_value(new Variant("(u)", return_val));
                break;
            case "AddUserActiveWatch":
                var return_val = add_user_active_watch();
                invocation.return_value(new Variant("(u)", return_val));
                break;
            case "ResetIdletime":
                reset_idletime();
                invocation.return_value(null);
                break;
            case "RemoveWatch":
                uint32 watch_id;
                parameters.get("(u)", out watch_id);
                remove_watch(watch_id);
                invocation.return_value(null);
                break;
            default:
                invocation.return_gerror(new DBusError.UNKNOWN_METHOD(method_name));
                break;
        }
    }

    // Impl.

    private uint64 idletime = 0;
    private uint32 last_id = 0; // Last ID used to register a watch
    private uint tick_source = 0;

    // Watch storage
    class IdleWatch {
        public uint32 id;
        public uint64 interval;
        public uint64 start_time;
    }
    private List<IdleWatch> idle_watches = new List<IdleWatch>();
    private List<uint32> user_active_watches = new List<uint32>();

    private void _on_idletime_tick() {
        idletime += 10; // We tick 10 times a second


        if (tick_source == 0) {
            tick_source = Timeout.add(10, () => {
                _on_idletime_tick();
                return Source.CONTINUE;
            });
        }

        // Fire the idle watches
        foreach (var watch in idle_watches)
            if ((idletime - watch.start_time) % watch.interval == 0)
                watch_fired(watch.id);
    }

    private void _on_user_activity() {
        reset_idletime();

        if (tick_source != 0) {
            Source.remove(tick_source);
            tick_source = 0;
        }

        // Fire the user activity watches
        foreach (var watch in user_active_watches)
            watch_fired(watch);
    }

    public signal void watch_fired(uint32 id);

    public uint64 get_idletime() {
        return idletime;
    }

    public uint32 add_idle_watch(uint64 interval) {
        var watch = new IdleWatch();
        watch.id = ++last_id;
        watch.interval = interval;
        watch.start_time = idletime;
        idle_watches.append(watch);
        return watch.id;
    }

    public uint32 add_user_active_watch() {
        user_active_watches.append(++last_id);
        return last_id;
    }

    public void remove_watch(uint32 id) {
        foreach (var watch in idle_watches)
            if (watch.id == id) {
                idle_watches.remove(watch);
                return;
            }
        foreach (var watch in user_active_watches)
            if (watch == id) {
                user_active_watches.remove(watch);
                return;
            }
    }

    public void reset_idletime() {
        foreach (var watch in idle_watches) // Adjust the idle watches
            watch.start_time = 0;
        idletime = 0; // Reset the idle time
    }
}
