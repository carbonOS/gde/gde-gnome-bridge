// Common display sizes to round to
const double[] KNOWN_DISPLAY_SIZES = { 12.1, 13.3, 15.6 };

// Manufacturer info
private Gnome.PnpIds? pnp_ids = null;
string? get_make_display_name(string pnp_id) {
    if (pnp_ids == null) pnp_ids = new Gnome.PnpIds();
    return pnp_ids.get_pnp_id(pnp_id);
}

// Scaling & modes

const double DEFAULT_UI_SCALE_DPI = 256.6;
const double MIN_SCALE_FACTOR = 1.0;
const int SCALE_FACTORS_PER_INT = 4;
const double SCALE_FACTOR_STEP = (1.0 / SCALE_FACTORS_PER_INT);
const double MAX_SCALE_FACTOR = 4.0;
const int MIN_LOGICAL_AREA = (800 * 480);

double closest_scale_for_resolution(double w, double h, double scale,
                                    double threshold) {
    var best_scale = 0.0;
    var scaled_w = w / scale;
    var scaled_h = h / scale;

    // Don't ever scale too small
    if (Math.floor(scaled_w) * Math.floor(scaled_h) < MIN_LOGICAL_AREA)
        return best_scale;

    // If the scale is already good, return it
    if (Math.floor(scaled_w) == scaled_w && Math.floor(scaled_h) == scaled_h)
        return scale;

    var found_one = false;
    var i = 0;
    var base_scaled_w = Math.floor(scaled_w);
    do {
        for (var j = 0; j < 2; j++) {
            var offset = i * (j > 0 ? 1.0 : -1.0);

            scaled_w = base_scaled_w + offset;
            var current_scale = w / scaled_w;

            // Any decimals less than 0.0001 will always resolve to less than a
            // pixel. We truncate the scale to ensure that we don't find anything
            // more precise than 0.0001
            current_scale = Math.floor(current_scale * 10000) / 10000;

            if (current_scale >= scale + threshold ||
                current_scale < scale - threshold ||
                current_scale < MIN_SCALE_FACTOR ||
                current_scale > MAX_SCALE_FACTOR)
                return best_scale;

            scaled_h = h / current_scale;
            if (Math.floor(scaled_h) == scaled_h) {
                found_one = true;
                if (Math.fabs(current_scale - scale) < Math.fabs(best_scale - scale))
                    best_scale = current_scale;
            }
        }

        i++;
    } while (!found_one);

    return best_scale;
}

// wlr-output-management cannot give us the proper scale of the output, because
// wl_fixed_t cannot represent our scale values with enough precision. For
// instance, a scale of 1.6 gets represented as approx 1.60156. So, for the
// scale we're given from wlr-output-management, we try to find the true scale
// value we're using to ensure that we're not reporting incorrect data to our
// clients (g-c-c).
double pick_actual_scale(Output output) {
    var possible_scales = output.current_mode.get_supported_scales();
    foreach (var candidate in possible_scales)
        if ((output.scale - candidate).abs() <= 0.1)
            return candidate;
    return output.scale; // Fallback to the value wlr-output-management gave us
}

List<unowned Mode> filter_modes(List<Mode> unfiltered) {
    var filtered = new HashTable<string, Mode>(str_hash, str_equal);
    foreach (var mode in unfiltered) {
        // Skip if too small
        if (mode.width * mode.height < MIN_LOGICAL_AREA)
            continue;

        // Skip if duplicate (but prefer the preferred mode always)
        var mode_id = mode.to_string();
        if (mode_id in filtered && !mode.preferred)
            continue;

        // Add the mode
        filtered[mode_id] = mode;
    }
    return filtered.get_values();
}
