[DBus (name = "org.gnome.Mutter.DisplayConfig")]
class DisplayConfigService : Object {
    private uint32 current_serial = 0;
    private GSettingsMgr settings;
    private AutorotateMgr autorotate;

    public DisplayConfigService(DBusConnection conn) {
        try {
            conn.register_object("/org/gnome/Mutter/DisplayConfig", this);
        } catch {
            warning("Failed to register DisplayConfigService");
            return;
        }

        settings = new GSettingsMgr();
        autorotate = new AutorotateMgr(settings);

        this.monitors_changed.connect(on_monitors_changed_internal);
        settings.primary_changed.connect(() => monitors_changed());
        DisplayConfig.setup(() => monitors_changed());
    }

    public void get_resources(out uint32 serial,
                              out GR_CRTC[] crtcs,
                              out GR_Output[] outputs,
                              out GR_Mode[] modes,
                              out int32 max_screen_width,
                              out int32 max_screen_height) throws Error {
        serial = current_serial; // Return the current serial

        // Temporary arrays so that we can dynamically add to them
        GR_CRTC[] out_crtcs = {};
        GR_Output[] out_outputs = {};
        GR_Mode[] out_modes = {};
        var all_outputs = DisplayConfig.get_outputs();

        string primary_connector = settings.get_primary_monitor();
        foreach (var output in all_outputs) {
            var output_id = output.hash();

            // Make the CRTCs for the output
            if (output.enabled) {
                var crtc = GR_CRTC() {
                    id = output_id, winsys_id = 0,
                    x = output.x, y = output.y,
                    width = output.current_mode.width,
                    height = output.current_mode.height,
                    mode = (int32) output.current_mode.to_string().hash(),
                    current_transform = output.transform,
                    transforms = { 0, 1, 2, 3, 4, 5, 6, 7 }, // All transforms
                    properties = new HashTable<string, Variant>(str_hash, str_equal)
                };
                out_crtcs += crtc;
            }

            // Make the modes for the output
            uint32[] possible_modes = {};
            foreach (var mode in output.modes) {
                var mode_id = mode.to_string().hash();
                possible_modes += mode_id;
                var out_mode = GR_Mode() {
                    id = mode_id, winsys_id = 0,
                    width = mode.width, height = mode.height,
                    frequency = mode.refresh_rate,
                    flags = 0 // no flags
                };
                out_modes += out_mode;
            }

            // Find the clones of the output
            uint32[] clones = {};
            foreach (var clone in all_outputs) {
                if (clone == output) continue; // Skip itself
                if (clone.x == output.x && clone.y == output.y)
                    clones += clone.hash();
            }

            // Create the additional properties map
            var props = new HashTable<string, Variant>(str_hash, str_equal);
            props["vendor"] = output.make;
            props["product"] = output.model;
            props["serial"] = output.serial_number;
            props["width-mm"] = output.width_mm;
            props["height-mm"] = output.height_mm;
            props["display-name"] = output.make_display_name();
            props["connector-type"] = output.connector.split("-", 2)[0];
            props["backlight"] = -1; // Unsupported in this proto
            props["primary"] = (output.connector == primary_connector);
            props["presentation"] = false;
            props["underscanning"] = false;
            props["supports-underscanning"] = false;
            props["supports-color-transform"] = false;

            var out_output = GR_Output() {
                id = output_id, winsys_id = 0,
                current_crtc = (int32)(output.enabled ? output_id : -1),
                possible_crtcs = { output_id },
                name = output.connector,
                modes = possible_modes,
                clones = clones,
                properties = props
            };
            out_outputs += out_output;
        }

        // Calculate max width/height
        uint32 max_w = 0, max_h = 0;
        foreach (var mode in out_modes) {
            if (mode.width > max_w)
                max_w = mode.width;
            if (mode.height > max_h)
                max_h = mode.height;
        }

        // Return the outputs
        crtcs = out_crtcs;
        outputs = out_outputs;
        modes = out_modes;
        max_screen_width = (int32) max_w;
        max_screen_height = (int32) max_h;
    }

    public void apply_configuration(uint32 serial,
                                    bool persistent,
                                    AC_CRTC[] crtcs,
                                    AC_Output[] outputs) throws Error {
        if (serial != current_serial)
            throw new DBusError.ACCESS_DENIED("Request based on stale info");

        // REASONING: This is unused in both g-c-c and g-s-d, so it's safe to
        // leave unimplemented
    }

    public int32 change_backlight(uint32 serial, uint32 output,
                                  int32 val) throws Error {
        if (serial != current_serial)
            throw new DBusError.ACCESS_DENIED("Request based on stale info");

        // REASONING: g-s-d manages backlight and this is only used as a
        // fallback if logind is not available to manage the brightness. If
        // logind is not available, we have much bigger problems than not being
        // able to adjust screen brightness

        return 100; // If anything does end up calling this, return 100%
    }

    public void get_crtc_gamma(uint32 serial, uint32 crtc,
                               out uint16[] red,
                               out uint16[] green,
                               out uint16[] blue) throws Error {
        if (serial != current_serial)
            throw new DBusError.ACCESS_DENIED("Request based on stale info");

        // Find the output by ID number
        Output? output = null;
        foreach (var candidate in DisplayConfig.get_outputs())
            if (candidate.hash() == crtc) {
                output = candidate;
                break;
            }
        if (output == null)
            throw new DBusError.INVALID_ARGS("Invalid CRTC ID");


        // Create the output (see REASONING)
        red = new uint16[output.gamma_size];
        for (int i = 0; i < output.gamma_size; i++) red[i] = 0;
        green = {};
        blue = {};

        // REASONING:
        // GetCrtcGamma is only ever called once in gnome-settings-daemon,
        // to find the size of the gamma ramps for an output. This happens
        // by calling GetCrtcGamma and then checking the length of the red
        // array. The actual values of the ramps are thrown out
    }

    public void set_crtc_gamma(uint32 serial, uint32 crtc,
                               uint16[] red,
                               uint16[] green,
                               uint16[] blue) throws Error {
        if (serial != current_serial)
            throw new DBusError.ACCESS_DENIED("Request based on stale info");

        // Find the output by ID number
        Output? output = null;
        foreach (var candidate in DisplayConfig.get_outputs())
            if (candidate.hash() == crtc) {
                output = candidate;
                break;
            }
        if (output == null)
            throw new DBusError.INVALID_ARGS("Invalid CRTC ID");

        // Set the gamma on the output
        DisplayConfig.set_gamma(output, red, green, blue);
    }

    private PowerSaveMode _dpms_mode = PowerSaveMode.UNKNOWN;
    public PowerSaveMode power_save_mode {
        get {
            return _dpms_mode;
        }
        set {
            _dpms_mode = value;
            DisplayConfig.set_power_mode(value);
        }
    }

    public bool panel_orientation_managed {
        get {
            return autorotate.managing_rotation;
        }
    }

    public bool apply_monitors_config_allowed {
        get {
            return true;
        }
    }

    public bool night_light_supported {
        get {
            foreach (var output in DisplayConfig.get_outputs())
                if (output.gamma_size == 0)
                    return false;
            return true;
        }
    }

    public signal void monitors_changed();

    private void on_monitors_changed_internal() {
        current_serial++;

        // Set power mode to the power of the first output
        var outputs = DisplayConfig.get_outputs();
        if (outputs.length > 0) {
            _dpms_mode = outputs[0].power_mode;
        } else {
            _dpms_mode = PowerSaveMode.UNKNOWN;
        }

        // Update the settings based on the state hash
        var hash = DisplayConfig.get_current_state_hash();
        var old_hash = settings.hash;
        if (hash != old_hash) {
            settings.hash = hash;

            if (!settings.get_hash_seen() && old_hash != "STARTING") {
                info("New hash: %s; importing settings from %s", hash, old_hash);
                settings.import_from_hash(old_hash, outputs);
            }

            info("Switch to display hash: %s", hash);
            settings.resync(outputs);
        }

        // Update the rotation settings
        autorotate.update(outputs);
    }

    public void get_current_state(out uint32 serial,
                                  out GCS_Monitor[] monitors,
                                  out GCS_LogicalMonitor[] logical_monitors,
                                  out HashTable<string, Variant> properties)
                                  throws Error {
        serial = current_serial; // Return the current serial

        GCS_Monitor[] out_monitors = {};
        GCS_LogicalMonitor[] out_lmonitors = {};

        string primary_connector = settings.get_primary_monitor();
        foreach (var output in DisplayConfig.get_outputs()) {
            var id = GCS_Monitor_ID() {
                connector = output.connector,
                vendor = output.make,
                product = output.model,
                serial = output.serial_number
            };

            GCS_Mode[] modes = {};
            foreach (var mode in output.modes) {
                var props = new HashTable<string, Variant>(str_hash, str_equal);
                props["is-current"] = (mode == output.current_mode);
                props["is-preferred"] = mode.preferred;
                props["is-interlaced"] = false;

                var monitor_mode = GCS_Mode() {
                    id = mode.to_string(),
                    width = mode.width, height = mode.height,
                    refresh_rate = mode.refresh_rate,
                    preferred_scale = mode.get_default_scale(output),
                    supported_scales = mode.get_supported_scales(),
                    properties = props
                };
                modes += monitor_mode;
            }

            var props = new HashTable<string, Variant>(str_hash, str_equal);
            props["display-name"] = output.make_display_name();
            props["is-builtin"] = output.is_builtin();
            props["width-mm"] = output.width_mm;
            props["height-mm"] = output.height_mm;

            var monitor = GCS_Monitor() {
                id = id, modes = modes, properties = props
            };
            out_monitors += monitor;

            // If the display is disabled, it has no logical monitor.
            if (!output.enabled) continue;

            // A logical monitor for this output might potentially exist already
            // Try to look it up
            GCS_LogicalMonitor* lmonitor = null;
            foreach (var candidate in out_lmonitors)
                if (candidate.x == output.x && candidate.y == output.y) {
                    lmonitor = &candidate;
                    break;
                }
            if (lmonitor == null) {
                // No matching logical monitor was found. Make one
                var new_lmonitor = GCS_LogicalMonitor() {
                    x = output.x, y = output.y,
                    scale = pick_actual_scale(output),
                    transform = output.transform,
                    primary = false, // potentially updated later
                    monitors = { id },
                    properties = new HashTable<string, Variant>(str_hash, str_equal)
                };
                out_lmonitors += new_lmonitor;
                lmonitor = &out_lmonitors[out_lmonitors.length - 1];
            } else {
                // We found an existing logical monitor. Add ourselves to it
                var workaround = lmonitor.monitors;
                workaround += id; // vala quirk doesn't let us use += directly
                lmonitor.monitors = workaround;
            }
            if (output.connector == primary_connector)
                lmonitor.primary = true;
        }

        monitors = out_monitors;
        logical_monitors = out_lmonitors;

        // Set up the properties
        properties = new HashTable<string, Variant>(str_hash, str_equal);
        properties["layout-mode"] = 1u;
        properties["supports-changing-layout-mode"] = false;
        properties["global-scale-required"] = false;
        properties["legacy-ui-scaling-factor"] = 1; // TODO
    }

    public void apply_monitors_config(uint32 serial,
                                      uint32 method,
                                      AMC_LogicalMonitor[] logical_monitors,
                                      HashTable<string, Variant> properties)
                                      throws Error {
        if (serial != current_serial)
            throw new DBusError.ACCESS_DENIED("Request based on stale info");

        if (method == 0) // If mode = "verify", do nothing
            return;

        var outputs = DisplayConfig.get_outputs();

        foreach (var output in outputs) {
            var store = settings.get_store(output.connector);
            store.set_string("mode", "off");
            store.set_boolean("primary", false);
        }

        foreach (var logical in logical_monitors) {
            var new_transform = "normal";
            switch (logical.transform) {
                case 1:
                    new_transform = "90";
                    break;
                case 2:
                    new_transform = "180";
                    break;
                case 3:
                    new_transform = "270";
                    break;
                case 4:
                    new_transform = "flipped";
                    break;
                case 5:
                    new_transform = "90_flipped";
                    break;
                case 6:
                    new_transform = "180_flipped";
                    break;
                case 7:
                    new_transform = "270_flipped";
                    break;
            }
            var new_pos = "%i,%i".printf(logical.x, logical.y);

            string? prev_connector = null;
            foreach (var monitor in logical.monitors) {
                var store = settings.get_store(monitor.connector);

                var new_mode = monitor.mode_id;
                if (prev_connector != null)
                    new_mode = "mirror %s".printf(prev_connector);
                else {
                    prev_connector = monitor.connector;
                    if (logical.primary) store.set_boolean("primary", true);
                }

                store.set_string("position", new_pos);
                store.set_string("mode", new_mode);
                store.set_double("scale", logical.scale);
                store.set_string("transform", new_transform);
            }
        }

        settings.resync(outputs);
    }
}

