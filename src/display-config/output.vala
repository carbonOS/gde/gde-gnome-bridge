public class Mode : Object {
    public int32 width { get; protected set; }
    public int32 height { get; protected set; }
    public double refresh_rate { get; protected set; }
    public bool preferred { get; protected set; default = false; }

    public double[] get_supported_scales() {
        double[] supported_scales = {};
        for (var i = Math.floor(MIN_SCALE_FACTOR);
                 i <= Math.ceil(MAX_SCALE_FACTOR);
                 i++) {
            for (var j = 0; j < SCALE_FACTORS_PER_INT; j++) {
                var raw_scale = i + j * SCALE_FACTOR_STEP;
                var scale = closest_scale_for_resolution((double) this.width,
                    (double) this.height, raw_scale, SCALE_FACTOR_STEP / 2.0);
                if (scale > 0.0 && !(scale in supported_scales))
                    supported_scales += scale;
            }
        }
        if (supported_scales.length == 0) supported_scales += 1.0; // Fallback
        return supported_scales;
    }

    public double get_default_scale(Output output) {
        var scales = get_supported_scales();
        var best = scales[0]; // Default to the smallest possible scale
        var best_dpi_y = (width / best) / (output.height_mm / 25.4);

        // We can't do much in this case
        if (output.has_aspect_as_size()) return best;

        foreach (var scale in scales) {
            var scale_dpi_y = (width / scale) / (output.height_mm / 25.4);
            if ((scale_dpi_y - DEFAULT_UI_SCALE_DPI).abs() <
                (best_dpi_y - DEFAULT_UI_SCALE_DPI).abs()) {
                best = scale;
                best_dpi_y = scale_dpi_y;
            }
        }

        return best;
    }

    public string to_string() {
        return "%dx%d@%d".printf(width, height, (int)(refresh_rate * 1000));
    }

    public bool is_better_than(Mode other) {
        if (this.preferred) return true;
        if (other.preferred) return false;

        var our_size = this.width * this.height;
        var their_size = other.width * other.height;
        if (our_size > their_size) {
            // Our resolution is better, but we'll prefer the smaller
            // resolution if we'd drop below 60Hz
            return this.refresh_rate > 59.9;
        } else if (our_size == their_size) {
            // If the resolution is equal, decide based on refresh rate alone
            return this.refresh_rate > other.refresh_rate;
        } else {
            // Their resolution is better, but we'll prefer the smaller
            // resolution if we'd drop below 60Hz
            return other.refresh_rate < 59.9;
        }
    }
}

public class Output : Object {
    public string connector { get; protected set; }
    public int32 width_mm { get; protected set; }
    public int32 height_mm { get; protected set; }
    public Mode current_mode { get; protected set; }
    public List<Mode> unfiltered_modes;
    public List<unowned Mode> modes {
        owned get { return filter_modes(this.unfiltered_modes); }
    }
    public bool enabled { get; protected set; }
    public int32 x { get; protected set; }
    public int32 y { get; protected set; }
    public int32 transform { get; protected set; }
    public double scale { get; protected set; }
    public string make { get; protected set; default = "unknown"; }
    public string model { get; protected set; default = "unknown"; }
    public string serial_number { get; protected set; default = "unknown"; }
    public uint32 gamma_size { get; protected set; }
    public PowerSaveMode power_mode { get; protected set; }

    // Used internally in C code
    public bool has_wl_output { get; protected set; }
    public void* wl_output { get; protected set; }
    public void* power_manager { get; protected set; }
    public void* gamma_manager { get; protected set; }

    public uint hash() {
        return "%s%s%s%s".printf(connector, make, model, serial_number).hash();
    }

    public Mode get_default_mode() {
        Mode best = null;
        foreach (var mode in this.modes) {
            if (best == null || mode.is_better_than(best))
                best = mode;
        }
        return best;
    }

    public double get_default_scale() {
        return this.current_mode.get_default_scale(this);
    }

    public bool is_builtin() {
        return connector.has_prefix("eDP-") ||
               connector.has_prefix("LVDS-") ||
               connector.has_prefix("DSI-");
    }

    public bool has_aspect_as_size() {
        return (width_mm == 1600 && height_mm == 900) ||
               (width_mm == 1600 && height_mm == 1000) ||
               (width_mm == 160 && height_mm == 90) ||
               (width_mm == 160 && height_mm == 100) ||
               (width_mm == 16 && height_mm == 9) ||
               (width_mm == 16 && height_mm == 10);
    }

    public string make_display_name() {
        if (is_builtin()) return "Built-in display";

        string? inches = null;
        string? vendor_name = null;

        if (width_mm > 0 && height_mm > 0) {
            if (!has_aspect_as_size()) {
                double d = Math.sqrt(width_mm * width_mm +
                                     height_mm * height_mm) / 25.4;
                foreach (var size in KNOWN_DISPLAY_SIZES) {
                    if ((size - d).abs() < 0.1)
                        inches = "%0.1lf\"".printf(size);
                }
                if (inches == null) inches = "%d\"".printf((int)(d + 0.5));
            }
        }

        if (make != "Unknown") {
            vendor_name = get_make_display_name(make);
            if (vendor_name == null) vendor_name = make;
        } else if (connector.has_prefix("Virtual-")) {
            vendor_name = "Virtual"; // VMs that don't pass though make/model
        } else {
            vendor_name = "Unknown"; // TODO: For translation, eventually
        }

        if (inches != null) {
            return "%s %s".printf(make, inches);
        } else {
            return "%s %s".printf(make, model);
        }
    }
}


