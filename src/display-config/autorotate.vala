[DBus(name="net.hadess.SensorProxy")]
interface SensorProxy : DBusProxy {
    public abstract bool has_accelerometer { get; }
    public abstract string accelerometer_orientation { owned get; }
    public abstract void claim_accelerometer() throws Error;
    public abstract void release_accelerometer() throws Error;
}

class AutorotateMgr {
    public bool managing_rotation = false;

    private SensorProxy sensor_proxy = null;
    private Settings gnome_settings;
    private GSettingsMgr output_settings;
    private Output? builtin_output;

    public AutorotateMgr(GSettingsMgr settings) {
        output_settings = settings;
        gnome_settings = new Settings("org.gnome.settings-daemon.peripherals.touchscreen");
        gnome_settings.changed["orientation-lock"].connect(() => {
           update(DisplayConfig.get_outputs());
        });

        Bus.watch_name(BusType.SYSTEM, "net.hadess.SensorProxy",
            BusNameWatcherFlags.NONE, on_appeared, on_vanished);
    }

    private void on_appeared(DBusConnection conn, string name, string owner) {
        // Try to get the proxy
        try {
            sensor_proxy = conn.get_proxy_sync(name, "/net/hadess/SensorProxy");
            info("Sensor proxy appeared");
        } catch {
            warning("Couldn't get autorotate sensor proxy");
            managing_rotation = false;
            return;
        }

        // Listen for rotation notifications
        sensor_proxy.g_properties_changed.connect((props, inval) => {
            var table = (HashTable<string, Variant>) props;
            if ("AccelerometerOrientation" in table) set_rotation();
        });

        // Update the orientation status
        update(DisplayConfig.get_outputs());
    }

    private void on_vanished(DBusConnection conn, string name) {
        sensor_proxy = null;
        managing_rotation = false;
        info("Sensor proxy disappeared");
    }

    private void enable() {
        try {
            if (sensor_proxy != null) {
                sensor_proxy.claim_accelerometer();
                managing_rotation = true;
                return;
            }
        } catch {}
        warning("Couldn't claim accelerometer");
    }

    private void disable() {
        try {
            if (sensor_proxy != null) {
                sensor_proxy.release_accelerometer();
                managing_rotation = false;
                return;
            }
        } catch {}
        warning("Couldn't release accelerometer");
    }

    public void update(Output[] outputs) {
        // No sensor proxy means no autorotation
        if (sensor_proxy == null) return;

        // Handle orientation-lock setting or lack of sensor
        if (gnome_settings.get_boolean("orientation-lock") ||
            !sensor_proxy.has_accelerometer) {
            disable();
            return;
        }

        // Try to find a built-in output
        builtin_output = null;
        foreach (var candidate in outputs)
            if (candidate.is_builtin()) {
                builtin_output = candidate;
                break;
            }

        // Enable/disable autorotation accordingly
        if (builtin_output == null || !builtin_output.enabled)
            disable();
        else
            enable();

        set_rotation(outputs); // Update the rotation if necessary
    }

    private void set_rotation(Output[] outputs = DisplayConfig.get_outputs()) {
        if (builtin_output == null || !managing_rotation) return;

        // Convert accel orientation to a transform
        string new_transform;
        switch (sensor_proxy.accelerometer_orientation) {
            case "bottom-up":
                new_transform = "180";
                break;
            case "left-up":
                new_transform = "90";
                break;
            case "right-up":
                new_transform = "270";
                break;
            case "normal":
            case "undefined":
            default:
                new_transform = "normal";
                break;
        }

        // Do the setting change
        var store = output_settings.get_store(builtin_output.connector);
        store.set_string("transform", new_transform);
        output_settings.resync(outputs);
    }
}
