namespace DisplayConfig {

    private List<Output> outputs;

    public Output[] get_outputs() {
        Output[] ret = {};
        foreach (var output in outputs)
            ret += output;
        return ret;
    }

    // Returns a string that uniquely IDs this exact configuration of monitors
    public string get_current_state_hash() {
        var list = new List<string>();
        foreach (var output in outputs)
            list.append("%u".printf(output.hash()));
        list.sort(strcmp);

        string bld = "";
        foreach (var s in list)
            bld += s;
        return bld.hash().to_string("%x");
    }

    public void set_power_mode(PowerSaveMode mode) {
        if (mode == PowerSaveMode.UNKNOWN) return;
        foreach (var output in outputs)
            do_set_power_mode(output, mode == PowerSaveMode.ON);
    }

    public delegate void OutputChangedSignal();
    private OutputChangedSignal? on_outputs_changed = null;
    public void setup(owned OutputChangedSignal callback) {
        if (on_outputs_changed != null) return;
        on_outputs_changed = (owned) callback;
        outputs = new List<Output>();
        connect();
    }

    // C Interop functions

    public void emit_outputs_changed() {
        on_outputs_changed();
    }

    public Output create_output_obj(void* handle) {
        var output = new Output();
        output.unfiltered_modes = new List<Mode>();
        outputs.append(output);
        return output;
    }

    public void destroy_output_obj(Output obj) {
        outputs.remove(obj);
        obj.unref();
    }

    public Mode create_mode_obj() {
        return new Mode();
    }

    public void destroy_mode_obj(Mode mode) {
        foreach (var output in outputs) {
            bool contains = false;
            foreach (var m in output.unfiltered_modes) {
                if (m == mode) {
                    contains = true;
                    break;
                }
            }
            if (contains) output.unfiltered_modes.remove(mode);
        }
    }

    public void add_mode_to_output(Output output, Mode mode) {
        output.unfiltered_modes.append(mode);
    }

    bool ensuring_power_mode = false;
    public void ensure_all_outputs_match_power_mode(Output match) {
        if (ensuring_power_mode) return;

        ensuring_power_mode = true;
        foreach (var output in outputs)
            if (output.power_mode != match.power_mode)
                do_set_power_mode(output, match.power_mode == PowerSaveMode.ON);
        ensuring_power_mode = false;
    }

    public void update_aux_protos() {
        foreach (var output in outputs) update_output_aux_protocols(output);
    }

    extern void connect();
    extern void do_set_power_mode(Output output, bool on);
    public extern void set_gamma(Output output, uint16 *r, uint16 *g, uint16 *b);
    extern void update_output_aux_protocols(Output output);
}

