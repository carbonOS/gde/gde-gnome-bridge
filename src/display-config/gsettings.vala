class GSettingsMgr {
    private Settings main_settings;
    private string[] seen_hashes;

    public string hash = "STARTING";

    public GSettingsMgr() {
        main_settings = new Settings("sh.carbon.gde-gnome-bridge.output");
        seen_hashes = main_settings.get_strv("seen-hashes");
        main_settings.changed["primary"].connect(() => primary_changed());
    }

    public signal void primary_changed();

    // Utility

    private Settings get_output(string connector) {
        return new Settings.with_path("org.wayfire.section.output",
            "/org/wayfire/section/output/%s/".printf(connector));
    }

    public Settings get_store(string connector, string? custom_hash = null) {
        var p = "/sh/carbon/gde-gnome-bridge/output/store/%s/%s/".printf(
            custom_hash ?? hash, connector);
        return new Settings.with_path("sh.carbon.gde-gnome-bridge.output.store", p);
    }

    public string get_primary_monitor() {
        return main_settings.get_string("primary");
    }

    // Sync

    public bool get_hash_seen() {
        var seen = hash in seen_hashes;
        if (!seen) {
            seen_hashes += hash;
            main_settings.set_strv("seen-hashes", seen_hashes);
        }
        return seen;
    }

    public void import_from_hash(string old_hash, Output[] outputs) {
        foreach (var output in outputs) {
            var old_store = get_store(output.connector, old_hash);
            var store = get_store(output.connector);

            store.set_string("mode", old_store.get_string("mode"));
            store.set_string("position", old_store.get_string("position"));
            store.set_double("scale", old_store.get_double("scale"));
            store.set_string("transform", old_store.get_string("transform"));
        }
    }

    public void resync(Output[] outputs) {
        string[] sections = {};
        Output primary_monitor = null;
        Output primary_fallback = null;

        foreach (var output in outputs) {
            var store = get_store(output.connector);
            var wf_settings = get_output(output.connector);

            wf_settings.set_string("position", store.get_string("position"));
            wf_settings.set_string("transform", store.get_string("transform"));

            var scale = store.get_double("scale");
            var mode_id = store.get_string("mode");
            if (mode_id == "auto") {
                var mode = output.get_default_mode();
                mode_id = mode.to_string();
                scale = mode.get_default_scale(output);
            } else if (scale <= 0.0) {
                scale = output.get_default_scale();
            }
            wf_settings.set_string("mode", mode_id);
            wf_settings.set_double("scale", scale);

            sections += "output:%s".printf(output.connector);

            if (output.is_builtin() || primary_fallback == null)
                primary_fallback = output;
            if (store.get_boolean("primary"))
                primary_monitor = output;
        }

        new Settings("org.wayfire.gsettings").set_strv("dyn-sections", sections);

        if (primary_monitor == null) primary_monitor = primary_fallback;
        main_settings.set_string("primary", primary_monitor.connector);
        var primary_gtk3 = "%d,%d".printf(primary_monitor.x, primary_monitor.y);
        main_settings.set_string("primary-gtk3", primary_gtk3);
    }
}
