#include <glib-2.0/glib.h>
#include <glib-2.0/glib-object.h>
#include <gtk-3.0/gdk/gdkwayland.h>
#include <wayland-client.h>
#include <wayland-client-protocol.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/mman.h>

#include "wlr-output-management-unstable-v1-client-protocol.h"
#include "wlr-output-power-management-unstable-v1-client-protocol.h"
#include "wlr-gamma-control-unstable-v1-client-protocol.h"

static void noop() {} // Does nothing

// Global instances of wayland resources
static struct wl_display *wayland_display = NULL;
static struct zwlr_output_power_manager_v1 *power_manager = NULL;
static struct zwlr_gamma_control_manager_v1 *gamma_manager = NULL;

// Interop functions (see protocol.vala)
void display_config_emit_outputs_changed();
GObject* display_config_create_output_obj();
void display_config_destroy_output_obj(GObject *obj);
GObject* display_config_create_mode_obj();
void display_config_destroy_mode_obj(GObject *obj);
void display_config_add_mode_to_output(GObject *output, GObject *mode);
void display_config_ensure_all_outputs_match_power_mode(GObject *match);
void display_config_disconnect_aux_protocols_from_output(void *output);
void display_config_update_aux_protos();

// DPMS CONTROL

static void
destroy_output_power_manager(void *obj)
{
  struct zwlr_output_power_v1 *output_power_manager;
  g_object_get(G_OBJECT(obj), "power-manager", &output_power_manager, NULL);
  if (output_power_manager)
    zwlr_output_power_v1_destroy(output_power_manager);
  g_object_set(G_OBJECT(obj), "power-manager", NULL, NULL);
}

static void
on_power_mode(void *obj, struct zwlr_output_power_v1 *pwr, uint32_t mode)
{
  g_object_set(G_OBJECT(obj), "power-mode", mode ? 0 : 3, NULL);
}

static void
on_power_failed(void *obj, struct zwlr_output_power_v1 *pwr)
{
  destroy_output_power_manager(obj);
}

static struct zwlr_output_power_v1_listener power_listener = {
  .mode = &on_power_mode,
  .failed = &on_power_failed
};

void
display_config_do_set_power_mode(GObject *obj, gboolean new_on)
{
  struct zwlr_output_power_v1 *output_power_manager = NULL;
  g_object_get(obj, "power-manager", &output_power_manager, NULL);
  if (!output_power_manager) return; // We don't have a power manager

  zwlr_output_power_v1_set_mode(output_power_manager, new_on);
}

// GAMMA CONTROL

static int
create_gamma_anon_file(size_t size)
{
  char template[] = "/tmp/gde-gnome-bridge-XXXXXX";
  int fd = mkstemp(template);
  if (fd < 0) return -1; // Couldn't create the template file

  int ret;
  do {
    errno = 0;
    ret = ftruncate(fd, size);
  } while (errno == EINTR);
  if (ret < 0) {
    close(fd);
    return -1;
  }

  unlink(template);
  return fd;
}

static int
setup_gamma_table_map(uint32_t ramp_size, uint16_t **table)
{
  size_t tbl_size = ramp_size * 3 * sizeof(uint16_t);
  int fd = create_gamma_anon_file(tbl_size);
  if (fd < 0) return -1; // Couldn't create a file descriptor

  void *data = mmap(NULL, tbl_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (data == MAP_FAILED) {
    close(fd);
    return -1; // Couldn't map the memory
  }

  *table = data;
  return fd;
}

static void
destroy_gamma_controller(void *obj)
{
  struct zwlr_gamma_control_v1 *output_gamma_control;
  g_object_get(G_OBJECT(obj), "gamma-manager", &output_gamma_control, NULL);
  if (output_gamma_control)
    zwlr_gamma_control_v1_destroy(output_gamma_control);
  g_object_set(G_OBJECT(obj), "gamma-manager", NULL, "gamma-size", 0, NULL);
}

static void
on_gamma_size(void *obj, struct zwlr_gamma_control_v1 *gamma, uint32_t size)
{
  g_object_set(G_OBJECT(obj), "gamma-size", size, NULL);
}

static void
on_gamma_failed(void *obj, struct zwlr_gamma_control_v1 *gamma)
{
  destroy_gamma_controller(obj);
}

static struct zwlr_gamma_control_v1_listener gamma_listener = {
  .gamma_size = &on_gamma_size,
  .failed = &on_gamma_failed
};

void
display_config_set_gamma(GObject *obj, uint16_t *r, uint16_t *g, uint16_t *b)
{
  uint32_t gamma_size;
  struct zwlr_gamma_control_v1 *output_gamma_control = NULL;
  g_object_get(obj, "gamma-size", &gamma_size,
               "gamma-manager", &output_gamma_control, NULL);
  if (!output_gamma_control) return; // We don't have a gamma manager
  if (gamma_size == 0) return; // We don't have a gamma size

  // Create the gamma table
  uint16_t *gamma_table;
  int gamma_fd = setup_gamma_table_map(gamma_size, &gamma_table);
  if (gamma_fd < 0) return; // Nothing we can do
  for (int i = 0; i < gamma_size; i++) {
    gamma_table[i] = r[i];
    (gamma_table + gamma_size)[i] = g[i];
    (gamma_table + gamma_size * 2)[i] = b[i];
  }

  // Set the gamma
  zwlr_gamma_control_v1_set_gamma(output_gamma_control, gamma_fd);

  // Clean up
  munmap(gamma_table, gamma_size * 3 * sizeof(uint16_t));
  close(gamma_fd);
}

// LINKING OTHER PROTOCOLS -> HEAD

static void
on_wl_output_geometry(void *obj, struct wl_output *output,
                      int32_t x, int32_t y,
                      int32_t width_mm, int32_t height_mm,
                      int32_t subpixel,
                      const char *make, const char *model,
                      int32_t transform)
{
  char *obj_make, *obj_model;
  g_object_get(G_OBJECT(obj), "make", &obj_make, "model", &obj_model, NULL);

  if (strcmp(obj_make, make) == 0 && strcmp(obj_model, model) == 0)
    g_object_set(G_OBJECT(obj), "has-wl-output", TRUE, "wl-output", output, NULL);
  else
    wl_output_release(output); // We won't need this output anymore
}

static struct wl_output_listener find_output_event_listener = {
  .geometry = &on_wl_output_geometry,
  .mode = &noop,
  .done = &noop,
  .scale = &noop,
  .name = &noop,
  .description = &noop
};

static void
output_find_registry_add(void *obj, struct wl_registry *registry, guint32 id,
                         const char *interface, guint32 version)
{
  gboolean has_output;
  g_object_get(G_OBJECT(obj), "has-wl-output", &has_output, NULL);
  if (has_output) return; // We already have our output

  if (strcmp(interface, wl_output_interface.name) == 0) {
    struct wl_output *output = wl_registry_bind(registry, id, &wl_output_interface, version);
    wl_output_add_listener(output, &find_output_event_listener, obj);
    wl_display_roundtrip(wayland_display);
  }
}

static struct wl_registry_listener find_output_registry_listener = {
  &output_find_registry_add, NULL
};

void
display_config_update_output_aux_protocols(void *obj)
{
  g_object_set(G_OBJECT(obj), "has-wl-output", FALSE, NULL);

  struct wl_registry *registry = wl_display_get_registry(wayland_display);
  wl_registry_add_listener(registry, &find_output_registry_listener, obj);
  wl_display_roundtrip(wayland_display);
  wl_registry_destroy(registry);

  gboolean has_output;
  g_object_get(G_OBJECT(obj), "has-wl-output", &has_output, NULL);

  if (has_output) {
    struct wl_output *output;
    struct zwlr_output_power_v1 *output_power_manager;
    struct zwlr_gamma_control_v1 *output_gamma_control;
    g_object_get(G_OBJECT(obj), "wl-output", &output,
                 "power-manager", &output_power_manager,
                 "gamma-manager", &output_gamma_control, NULL);

    // Create the manager objects if necessary
    if (!output_power_manager) {
      output_power_manager =
        zwlr_output_power_manager_v1_get_output_power(power_manager, output);
      zwlr_output_power_v1_add_listener(output_power_manager, &power_listener, obj);
    }
    if (!output_gamma_control) {
      output_gamma_control =
        zwlr_gamma_control_manager_v1_get_gamma_control(gamma_manager, output);
      zwlr_gamma_control_v1_add_listener(output_gamma_control, &gamma_listener, obj);
    }

    // Store in the object & clean up
    g_object_set(G_OBJECT(obj), "power-manager", output_power_manager,
                 "gamma-manager", output_gamma_control,
                 "wl-output", NULL, NULL);
     wl_output_release(output);
  } else {
    destroy_output_power_manager(obj);
    destroy_gamma_controller(obj);
  }
}

// MODE

static void
on_mode_size(void *obj, struct zwlr_output_mode_v1 *mode, int32_t w, int32_t h)
{
  g_object_set(G_OBJECT(obj), "width", w, "height", h, NULL);
}

static void
on_mode_refresh(void *obj, struct zwlr_output_mode_v1 *mode, int32_t refresh)
{
  double hz = refresh / 1000.0; // Convert mHz -> Hz
  g_object_set(G_OBJECT(obj), "refresh-rate", hz, NULL);
}

static void
on_mode_preferred(void *obj, struct zwlr_output_mode_v1 *mode)
{
  g_object_set(G_OBJECT(obj), "preferred", TRUE, NULL);
}

static void
on_mode_destroy(void *obj, struct zwlr_output_mode_v1 *mode)
{
  display_config_destroy_mode_obj(obj);
  zwlr_output_mode_v1_destroy(mode);
}

static struct zwlr_output_mode_v1_listener mode_listener = {
  .size = &on_mode_size,
  .refresh = &on_mode_refresh,
  .preferred = &on_mode_preferred,
  .finished = &on_mode_destroy
};

// HEAD

static void
on_head_name(void *obj, struct zwlr_output_head_v1 *head, const char *name)
{
  g_object_set(G_OBJECT(obj), "connector", name, NULL);
}

static void
on_head_size(void *obj, struct zwlr_output_head_v1 *head, int32_t w, int32_t h)
{
  g_object_set(G_OBJECT(obj), "width-mm", w, "height-mm", h, NULL);
}

static void
on_head_mode(void *obj, struct zwlr_output_head_v1 *head,
             struct zwlr_output_mode_v1 *mode)
{
  GObject *mode_obj = display_config_create_mode_obj();
  zwlr_output_mode_v1_set_user_data(mode, mode_obj);
  zwlr_output_mode_v1_add_listener(mode, &mode_listener, mode_obj);
  display_config_add_mode_to_output(G_OBJECT(obj), mode_obj);
}

static void
on_head_current_mode(void *obj, struct zwlr_output_head_v1 *head,
                     struct zwlr_output_mode_v1 *mode)
{
  g_object_set(G_OBJECT(obj), "current-mode", zwlr_output_mode_v1_get_user_data(mode), NULL);
}

static void
on_head_enabled(void *obj, struct zwlr_output_head_v1 *head, int32_t enabled)
{
  g_object_set(G_OBJECT(obj), "enabled", enabled, NULL);
}

static void
on_head_pos(void *obj, struct zwlr_output_head_v1 *head, int32_t x, int32_t y)
{
  g_object_set(G_OBJECT(obj), "x", x, "y", y, NULL);
}

static void
on_head_transform(void *obj, struct zwlr_output_head_v1 *head, int32_t transform)
{
  g_object_set(G_OBJECT(obj), "transform", transform, NULL);
}

static void
on_head_scale(void *obj, struct zwlr_output_head_v1 *head, wl_fixed_t scale)
{
  g_object_set(G_OBJECT(obj), "scale", wl_fixed_to_double(scale), NULL);
}

static void
on_head_make(void *obj, struct zwlr_output_head_v1 *head, const char *make)
{
  g_object_set(G_OBJECT(obj), "make", make, NULL);
}

static void
on_head_model(void *obj, struct zwlr_output_head_v1 *head, const char *model)
{
  g_object_set(G_OBJECT(obj), "model", model, NULL);
}

static void
on_head_serial(void *obj, struct zwlr_output_head_v1 *head, const char *serial)
{
  g_object_set(G_OBJECT(obj), "serial-number", serial, NULL);
}

static void
on_head_destroyed(void *obj, struct zwlr_output_head_v1 *head)
{
  display_config_destroy_output_obj(obj);
  zwlr_output_head_v1_destroy(head);
}

static struct zwlr_output_head_v1_listener head_listener = {
  .name = &on_head_name,
  .description = &noop,
  .physical_size = &on_head_size,
  .mode = &on_head_mode,
  .current_mode = &on_head_current_mode,
  .enabled = &on_head_enabled,
  .position = &on_head_pos,
  .transform = &on_head_transform,
  .scale = &on_head_scale,
  .make = &on_head_make,
  .model = &on_head_model,
  .serial_number = &on_head_serial,
  .finished = &on_head_destroyed
};

// MANAGER

static void
on_new_head(void *data, struct zwlr_output_manager_v1 *mgr,
            struct zwlr_output_head_v1 *head)
{
  GObject *output = display_config_create_output_obj(head);
  zwlr_output_head_v1_add_listener(head, &head_listener, output);
}

static void
on_done(void *data, struct zwlr_output_manager_v1 *mgr, uint32_t serial)
{
  display_config_update_aux_protos();
  wl_display_roundtrip(wayland_display); // Ensure aux protos are up-to-date
  display_config_emit_outputs_changed();
}

static void
on_finished(void *data, struct zwlr_output_manager_v1 *mgr)
{
  g_error("Compositor destroyed output manager");
}

static struct zwlr_output_manager_v1_listener mgr_listener = {
  .head = &on_new_head,
  .done = &on_done,
  .finished = &on_finished
};

// REGISTRY & SETUP

static void
registry_add(gpointer data, struct wl_registry *registry,
             guint32 id, const char *interface, guint32 version)
{
  if (strcmp(interface, zwlr_output_manager_v1_interface.name) == 0) {
    struct zwlr_output_manager_v1 *mgr = wl_registry_bind(registry, id, &zwlr_output_manager_v1_interface, version);
    zwlr_output_manager_v1_add_listener(mgr, &mgr_listener, NULL);
  } else if (strcmp(interface, zwlr_output_power_manager_v1_interface.name) == 0) {
    power_manager = wl_registry_bind(registry, id, &zwlr_output_power_manager_v1_interface, version);
  } else if (strcmp(interface, zwlr_gamma_control_manager_v1_interface.name) == 0) {
    gamma_manager = wl_registry_bind(registry, id, &zwlr_gamma_control_manager_v1_interface, version);
  }
}

static struct wl_registry_listener registry_listener = { &registry_add, &noop };

void
display_config_connect()
{
  GdkDisplay *gdk_display = gdk_display_get_default();
  wayland_display = gdk_wayland_display_get_wl_display(gdk_display);
  struct wl_registry *registry = wl_display_get_registry(wayland_display);
  wl_registry_add_listener(registry, &registry_listener, NULL);
  wl_display_roundtrip(wayland_display);
}

