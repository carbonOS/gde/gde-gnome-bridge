// structs are marked public to get valac to shut up about unused variables

public struct GR_CRTC {
    uint32 id;
    int64 winsys_id;
    int32 x;
    int32 y;
    int32 width;
    int32 height;
    int32 mode;
    uint32 current_transform;
    uint32[] transforms;
    HashTable<string, Variant> properties;
}

public struct GR_Output {
    uint32 id;
    int64 winsys_id;
    int32 current_crtc;
    uint32[] possible_crtcs;
    string name;
    uint32[] modes;
    uint32[] clones;
    HashTable<string, Variant> properties;
}

public struct GR_Mode {
    uint32 id;
    int64 winsys_id;
    uint32 width;
    uint32 height;
    double frequency;
    uint32 flags;

    // Flags:
    // NONE = 0
    // INTERLACE = 1 << 4;
}

public struct AC_CRTC {
    uint32 id;
    int32 new_mode;
    int32 x;
    int32 y;
    uint32 transform;
    uint32[] outputs;
    HashTable<string, Variant> properties;
}

public struct AC_Output {
    uint32 id;
    HashTable<string, Variant> properties;
}

public struct GCS_Monitor_ID {
    string connector;
    string vendor;
    string product;
    string serial;
}

public struct GCS_Mode {
    string id;
    int32 width;
    int32 height;
    double refresh_rate;
    double preferred_scale;
    double[] supported_scales;
    HashTable<string, Variant> properties;
}

public struct GCS_Monitor {
    GCS_Monitor_ID id;
    GCS_Mode[] modes;
    HashTable<string, Variant> properties;
}

public struct GCS_LogicalMonitor {
    int32 x;
    int32 y;
    double scale;
    uint32 transform;
    bool primary;
    GCS_Monitor_ID[] monitors;
    HashTable<string, Variant> properties;
}

public struct AMC_Monitor {
    string connector;
    string mode_id;
    HashTable<string, Variant> properties;
    // Known properties:
    // - "enable_underscanning" (b): enable monitor underscanning; may only be set when underscanning is supported (see GetCurrentState).
}

public struct AMC_LogicalMonitor {
    int32 x;
    int32 y;
    double scale;
    uint32 transform;
    bool primary;
    AMC_Monitor[] monitors;
}

public enum PowerSaveMode {
    UNKNOWN = -1,
    ON = 0,
    STANDBY = 1,
    SUSPEND = 2,
    OFF = 3
}
