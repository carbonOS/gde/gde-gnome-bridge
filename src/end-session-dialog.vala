[DBus (name = "org.gnome.SessionManager.EndSessionDialog")]
public interface EndSessionDialog : Object {
    public signal void confirmed_logout();
    public signal void confirmed_reboot();
    public signal void confirmed_shutdown();
    public signal void canceled();
    public signal void closed();
    public abstract void open(uint type, uint timestamp, uint timeout, ObjectPath[] inhibitors) throws Error;
    public abstract void close() throws Error;
}

public class EndSessionDialogService : EndSessionDialog, Object {
    private EndSessionDialog? proxy = null;

    public EndSessionDialogService(DBusConnection conn) {
        try {
            conn.register_object("/org/gnome/SessionManager/EndSessionDialog",
                this as EndSessionDialog);
            Bus.watch_name(BusType.SESSION, "sh.carbon.gde.EndSessionDialog",
                BusNameWatcherFlags.NONE, on_proxy_appeared, on_proxy_vanished);
        } catch {
            warning("Failed to register EndSessionDialogService");
            return;
        }
    }

    private void on_proxy_appeared(DBusConnection conn, string name, string owner) {
        try {
            proxy = conn.get_proxy_sync<EndSessionDialog>(name, "/");
        } catch {
            warning("Failed to obtain EndSessionDialogService proxy");
            return;
        }

        // Connect the signals
        proxy.confirmed_logout.connect(() => confirmed_logout());
        proxy.confirmed_reboot.connect(() => confirmed_reboot());
        proxy.confirmed_shutdown.connect(() => confirmed_shutdown());
        proxy.canceled.connect(() => canceled());
        proxy.closed.connect(() => closed());
    }

    private void on_proxy_vanished(DBusConnection conn, string name) {
        proxy = null;
    }

    public void open(uint type, uint timestamp, uint timeout, ObjectPath[] inhibitors) throws Error {
        if (proxy == null) {
            this.canceled();
            return;
        }

        proxy.open(type, timestamp, timeout, inhibitors);
    }


    public void close() throws Error {
        if (proxy != null)
            proxy.close();
    }
}

