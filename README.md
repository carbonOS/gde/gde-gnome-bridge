# gde-gnome-bridge

This bridges & translates several standard GNOME APIs into the APIs provided by
GDE, including implementations of GNOME DBus APIs.

## Building

Development is easiest on carbonOS through GNOME Builder. Simply switch
carbonOS to the devel variant:

```bash
$ updatectl switch --base=devel
[Enter your credentials at the prompt]
[Reboot]
```

and hit run in GNOME Builder. It's likely that carbonOS's included build of
gde-gnome-bridge will conflict with your custom build. To work around this, you can
terminate the system's gde-gnome-bridge whenver you're working on your changes:

```bash
$ systemctl --user stop gde-gnome-bridge
```

Once you're done working, you can restart the system's gde-gnome-bridge either
by reloading GDE (<kbd>Ctrl</kbd>+<kbd>R</kbd>, followed by `r` at the prompt)
or directly:

```bash
$ systemctl --user start gde-gnome-bridge
```

## APIs:

Everything (in parentheses) is still a TODO item.

- Links several GNOME gsettings into Wayfire
    - Input settings
    - (Lockdown settings)
- org.gnome.Shell
    - /org/gnome/SessionManager/EndSessionDialog: Proxy to gde-dialog's session dialog
    - /org/gnome/Shell: Main gnome-shell service
        - Monitor labler: Popup that shows monitor numbers when user is editing display settings
        - (Accel grab/ungrab): Proxy to gde-wf-plugins's keyboard grab interface
        - OSD: Popup that shows volume/brightness adjustments/other events
    - (Screenshooter -> xdg-desktop-portal): Service for taking screenshots
    - (Screen recorder -> xdg-desktop-portal): Service for recording the screen
    - (Remote Desktop -> ???): Service for requesting remote desktop control
    - (GTK Mount handler): ???
    - (Automount) (https://gitlab.gnome.org/GNOME/gnome-shell/-/blob/master/js/ui/components/automountManager.js)
- (org.gnome.Shell.ScreenTransition: Service to animate dark/light mode theme change)
- org.gnome.Mutter.DisplayConfig: Service for manipulating display settings
- org.gnome.Mutter.IdleMonitor: Service for keeping track of idle status
- (Screensaver)
